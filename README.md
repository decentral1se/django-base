[![pipeline status](https://git.coop/decentral1se/django-base/badges/master/pipeline.svg)](https://git.coop/decentral1se/django-base/commits/master)

# django-base

A Django [cookiecutter] template just for me.

[cookiecutter]: https://github.com/audreyr/cookiecutter
