#!/usr/bin/env python

import os

from {{ cookiecutter.project_slug }}.settings import Configuration

if __name__ == '__main__':
    os.environ['DJANGO_SETTINGS_MODULE'] = '{{ cookiecutter.project_slug }}.settings'
    Configuration.django_manage()
