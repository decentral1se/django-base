"""API views and viewsets.

See:
* https://www.django-rest-framework.org/api-guide/views/
* https://www.django-rest-framework.org/api-guide/generic-views/
* https://www.django-rest-framework.org/api-guide/viewsets/.
"""
