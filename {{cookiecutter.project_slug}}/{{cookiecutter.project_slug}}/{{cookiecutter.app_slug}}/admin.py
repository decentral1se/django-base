"""Admin models.

See https://docs.djangoproject.com/en/{{ cookiecutter.django_version }}/ref/contrib/admin/.
"""

from django.contrib import admin  # noqa
