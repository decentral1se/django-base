"""Database models.

See https://docs.djangoproject.com/en/{{ cookiecutter.django_version }}/ref/models/fields/.
"""

from django.db import models  # noqa
