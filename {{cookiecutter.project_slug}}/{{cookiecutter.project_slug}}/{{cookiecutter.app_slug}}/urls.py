"""API routes.

See https://www.django-rest-framework.org/api-guide/routers/.
"""

from rest_framework.routers import DefaultRouter

from {{cookiecutter.project_slug}}.{{cookiecutter.app_slug}} import views  # noqa

router = DefaultRouter()
