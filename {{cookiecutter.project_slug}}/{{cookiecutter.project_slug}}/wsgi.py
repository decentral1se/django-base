"""WSGI configuration.

See https://docs.djangoproject.com/en/{{ cookiecutter.django_version }}/howto/deployment/wsgi/.
"""

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
