"""Project settings.

See https://docs.djangoproject.com/en/{{ cookiecutter.django_version }}/topics/settings/.
"""

from os.path import abspath, dirname, join

import dj_database_url

from {{ cookiecutter.project_slug }}.goodconf import BaseConfiguration

BASE_DIR = dirname(dirname(abspath(__file__)))

Configuration = BaseConfiguration(
    load=True,
    default_files=[
        join(BASE_DIR, '{{ cookiecutter.project_slug }}', 'settings.yaml'),
    ],
)
