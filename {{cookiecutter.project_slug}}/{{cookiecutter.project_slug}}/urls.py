"""URL configuration.

See https://docs.djangoproject.com/en/{{ cookiecutter.django_version }}/topics/http/urls/.
"""

from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]
