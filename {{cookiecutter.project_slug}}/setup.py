"""Package configuration."""

from setuptools import find_packages, setup

version = '0.0.1'

setup(
    name='{{ cookiecutter.project_slug }}',
    version=version,
    packages=find_packages('.')
)
